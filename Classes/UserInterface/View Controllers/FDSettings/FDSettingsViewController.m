/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is the Alfresco Mobile App.
 *
 * The Initial Developer of the Original Code is Zia Consulting, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2011-2012
 * the Initial Developer. All Rights Reserved.
 *
 *
 * ***** END LICENSE BLOCK ***** */
//
//  FDSettingsViewController.m
//

#import "FDSettingsViewController.h"
#import "Theme.h"
#import "FDRowRenderer.h"
#import "FDSettingsPlistReader.h"

@implementation FDSettingsViewController
@synthesize settingsReader = _settingsReader;

- (void)dealloc
{
    [_settingsReader release];
    [super dealloc];
}

- (FDSettingsPlistReader *)settingsReader
{
    if(!_settingsReader)
    {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Root" ofType:@"plist"];
        [self setSettingsReader:[[[FDSettingsPlistReader alloc] initWithPlistPath:plistPath] autorelease]]; 
    }
    return _settingsReader;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [Theme setThemeForUINavigationBar:self.navigationController.navigationBar];

    [self setTitle:NSLocalizedString([self.settingsReader title],@"Settings View Title")];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

#pragma mark -
#pragma mark GenericViewController

- (void)constructTableGroups
{
    FDRowRenderer *rowRenderer = [[FDRowRenderer alloc] initWithSettings:self.settingsReader];
    rowRenderer.updateTarget = self;
    rowRenderer.updateAction = @selector(updateAction:);
    tableGroups = [[rowRenderer groups] retain];
	tableHeaders = [[rowRenderer headers] retain];
    [rowRenderer release];
}

-(void) updateAction:(id) sender
{
   
}
@end
